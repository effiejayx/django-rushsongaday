from django.contrib import admin
from . import models

# Register your models here

admin.site.register(models.Producer)
admin.site.register(models.Musician)
admin.site.register(models.Composition)
admin.site.register(models.Song)


class TrackAdmin(admin.ModelAdmin):
    list_filter = ['Album__title','Album__release_date']
    list_display = ('track_number','song', "Album",'duration')


class AlbumAdmin(admin.ModelAdmin):
    list_display = ('title','release_date','label', 'show_producers')
    def show_producers(self,obj):
        return " / ".join([a.name for a in obj.producer.all()])
    

admin.site.register(models.Album,AlbumAdmin)
admin.site.register(models.Track,TrackAdmin)