from django.db import models
from django.db.models.deletion import CASCADE, DO_NOTHING, SET_NULL

# Create your models here.


class Musician(models.Model):
    
    INSTRUMENT = (
            ('drums', 'drums'),
            ('guitar', 'guitar'),
            ('bass', 'bass'),
    )


    name = models.CharField(max_length=200)
    main_instrument = models.CharField(max_length=10,choices=INSTRUMENT) 
    def __str__(self):
        return self.name


class Composition(models.Model):
    lyrics=models.ManyToManyField(Musician, related_name="test")
    music=models.ManyToManyField(Musician, related_name="test2")

    def __str__(self):
        a = " & ".join([a.name for a in self.lyrics.all()])
        b = " & ".join([b.name for b in self.music.all()])
        if a == b:
            return "Lyrics & Music: %s " % (b)
        else: 
            return "Lyrics: %s & Music: %s " % (a,b)


class Producer(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        ordering = ['name']
    def __str__(self):
        return self.name

class Album(models.Model):

    LABELS = [
        ('moon','Moon'),
        ('anthem','Anthem'),
        ('mercury', 'Mercury'),
        ('anthemmercury', 'Anthem/Mercury'),
        ('anthemroadrunner', 'Anthem/Roadrunner'),
    ]


    title = models.CharField(max_length=200)
    release_date = models.DateField('date released')
    producer = models.ManyToManyField(Producer)
    label = models.CharField(max_length=25, choices=LABELS,null=True)
    class Meta:
        ordering = ['release_date']
    def __str__(self):
        return self.title


class Song(models.Model):
    title = models.CharField(max_length=100)
    written_by = models.ForeignKey(Composition, on_delete=DO_NOTHING, blank=True)
    class Meta:
        ordering = ['title']
    
    def __str__(self):
        return self.title

class Track(models.Model):
    track_number = models.IntegerField(null=True)
    song = models.ForeignKey(Song,on_delete=DO_NOTHING,null=True)
    Album = models.ForeignKey(Album,on_delete=DO_NOTHING,null=True)
    duration = models.DurationField(null=True)
 
    track_number.admin_order_field = 'track_number'

    def __str__(self):
        return "%d) %s - %s " % (self.track_number,self.song,self.duration)

class RushSongADay(models.Model):
    song=models.ForeignKey(Song,on_delete=DO_NOTHING)
    date=models.DateField(auto_now_add=True)
